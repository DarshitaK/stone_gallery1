# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20160713_1121'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=850)),
                ('name_url', models.CharField(default=None, max_length=850)),
                ('imglink', models.ImageField(max_length=2000, upload_to='static/images')),
                ('description', models.CharField(max_length=2000)),
            ],
        ),
        migrations.AlterField(
            model_name='product',
            name='category_id',
            field=models.ForeignKey(default=None, to='app.Category'),
        ),
    ]
