# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20160713_1636'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='heading',
            field=models.TextField(default=None),
        ),
    ]
