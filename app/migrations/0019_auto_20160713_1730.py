# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0018_auto_20160713_1722'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='img_url',
            field=models.ImageField(max_length=2000, upload_to='/static/images/category'),
        ),
        migrations.AlterField(
            model_name='product',
            name='imglink',
            field=models.ImageField(max_length=2000, upload_to='/static/images/product'),
        ),
        migrations.AlterField(
            model_name='top_six_category',
            name='img_url',
            field=models.ImageField(max_length=2000, upload_to='/static/images/category'),
        ),
    ]
