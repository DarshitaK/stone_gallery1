# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20160617_1357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='img_url',
            field=models.ImageField(max_length=2000, upload_to=b''),
        ),
    ]
