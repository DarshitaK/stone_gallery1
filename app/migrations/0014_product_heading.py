# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_remove_product_heading'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='heading',
            field=models.TextField(default=None),
        ),
    ]
