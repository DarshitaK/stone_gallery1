# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0020_auto_20160713_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carousel',
            name='img_url',
            field=models.ImageField(max_length=2000, upload_to='media/images/carousel'),
        ),
        migrations.AlterField(
            model_name='category',
            name='img_url',
            field=models.ImageField(max_length=2000, upload_to='media/images/category'),
        ),
        migrations.AlterField(
            model_name='top_six_category',
            name='img_url',
            field=models.ImageField(max_length=2000, upload_to='media/images/category'),
        ),
    ]
