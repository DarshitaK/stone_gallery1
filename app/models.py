from __future__ import unicode_literals

from django.db import models


# Create your models here.


class Carousel(models.Model):
    img_url = models.FileField(null= True,blank= True)
    text_heading = models.CharField(max_length=2000)
    text_para = models.CharField(max_length=2000)

   # def __str__(self):
    #    return self.

class Top_Six_Category(models.Model):
    name = models.CharField(max_length=850)
    name_url = models.CharField(max_length=850)
    img_url = models.ImageField(upload_to ='static/images/category',max_length=2000)
    description = models.CharField(max_length=2000)
    category_id = models.ForeignKey('app.Category', default=None)

    def __str__(self):
       return self.name

class Category(models.Model):
    name = models.CharField(max_length=850)
    name_url = models.CharField(max_length=850)
    img_url = models.ImageField(upload_to ='static/images/category',max_length=2000)
    description = models.CharField(max_length=2000)

    def __str__(self):
       return self.name

class Product(models.Model):
    name = models.CharField(max_length=850)
    name_url = models.CharField(max_length=850, default=None)
    price = models.FloatField()
    heading = models.TextField(default=None)
    imglink = models.ImageField(upload_to ='static/images/product',max_length=2000)
    description = models.CharField(max_length=2000)
    category_id = models.ForeignKey('app.Category', default=None)

    def __str__(self):
        return self.name

class Project(models.Model):
    name = models.CharField(max_length=850)
    name_url = models.CharField(max_length = 850, default= None)
    imglink = models.ImageField(upload_to='static/images/project', max_length=2000)
    description = models.CharField(max_length=2000)


class Featured_Category(models.Model):
    feat_cat_id = models.ForeignKey(Top_Six_Category, on_delete=models.CASCADE)

class Featured_Product(models.Model):
    feat_pro_id = models.ForeignKey(Product, on_delete=models.CASCADE)
