from django.contrib import admin

# Register your models here.
from .models import Carousel,Top_Six_Category,Product,Featured_Category,Featured_Product,Category,Project

admin.site.register(Carousel)
admin.site.register(Top_Six_Category)
admin.site.register(Product)
admin.site.register(Featured_Product)
admin.site.register(Featured_Category)
admin.site.register(Category)
admin.site.register(Project)