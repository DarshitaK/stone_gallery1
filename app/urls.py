from django.conf.urls import url
from .import views

urlpatterns =[
      url(r'^$',views.index, name="index"),
      url(r'^(?P<category_id>[0-9]+)/$',views.category, name="category"),
      #url(r'^(?P<category_id>[0-9]+)/(?P<product_id>[0-9]+)/$',views.product, name="prodcut"),
      url(r'^(?P<category_name_url>[a-zA-Z0-9]+)/(?P<product_name_url>[a-zA-Z0-9]+)/$',views.product, name="prodcut"),
      url(r'^(?P<category_name_url>[a-zA-Z0-9%]+)/$',views.category,name="xcategory"),
      url(r'^(?P<product_name_url>[a-zA-Z0-9%]+)/$',views.category,name="xcategory"),
      #url(r'^category/$',views.category, name="category"),
]